package ru.iteco.taskmanager.command;

import ru.iteco.taskmanager.bootstrap.Bootstrap;

public class MergeTaskCommand extends AbstractCommand {

	private Bootstrap bootstrap;
	
	public MergeTaskCommand(Bootstrap bootstrap) {
		this.bootstrap = bootstrap;
	}

	@Override
	public String command() {
		return "merge-task";
	}

	@Override
	public String description() {
		return "  -  merge task";
	}

	@Override
	public void execute() throws Exception {
		bootstrap.taskService.merge();
	}
}

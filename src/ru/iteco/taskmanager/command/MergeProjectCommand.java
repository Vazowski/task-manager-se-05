package ru.iteco.taskmanager.command;

import ru.iteco.taskmanager.bootstrap.Bootstrap;

public class MergeProjectCommand extends AbstractCommand{

	private Bootstrap bootstrap;
	
	public MergeProjectCommand(Bootstrap bootstrap) {
		this.bootstrap = bootstrap;
	}
	
	@Override
	public String command() {
		return "merge-project";
	}

	@Override
	public String description() {
		return "  -  merge project";
	}

	@Override
	public void execute() throws Exception {
		bootstrap.projectService.merge();
	}

}

package ru.iteco.taskmanager.command;

import ru.iteco.taskmanager.bootstrap.Bootstrap;

public class PersistProjectCommand extends AbstractCommand{
	
	private Bootstrap bootstrap;
	
	public PersistProjectCommand(Bootstrap bootstrap) {
		this.bootstrap = bootstrap;
	}

	@Override
	public String command() {
		return "persist-project";
	}

	@Override
	public String description() {
		return "  -  persist project";
	}

	@Override
	public void execute() throws Exception {
		bootstrap.projectService.persist();
	}
}

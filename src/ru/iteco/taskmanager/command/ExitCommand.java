package ru.iteco.taskmanager.command;

import ru.iteco.taskmanager.bootstrap.Bootstrap;

public class ExitCommand extends AbstractCommand {

	private Bootstrap bootstrap;
	
	public ExitCommand(Bootstrap bootstrap) {
		this.bootstrap = bootstrap;
	}
	
	@Override
	public String command() {
		return "exit";
	}

	@Override
	public String description() {
		return "  -  exit the programm";
	}

	@Override
	public void execute() throws Exception {
		System.exit(0);
	}

}

package ru.iteco.taskmanager.command;

import ru.iteco.taskmanager.bootstrap.Bootstrap;

public class FindAllTaskCommand extends AbstractCommand {

	private Bootstrap bootstrap;
	
	public FindAllTaskCommand(Bootstrap bootstrap) {
		this.bootstrap = bootstrap;
	}

	@Override
	public String command() {
		return "find-all-task";
	}

	@Override
	public String description() {
		return "  -  find all task in project";
	}

	@Override
	public void execute() throws Exception {
		bootstrap.taskService.findAll();
	}
}

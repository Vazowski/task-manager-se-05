package ru.iteco.taskmanager.command;

import ru.iteco.taskmanager.bootstrap.Bootstrap;

public class RemoveProjectCommand extends AbstractCommand {

private Bootstrap bootstrap;
	
	public RemoveProjectCommand(Bootstrap bootstrap) {
		this.bootstrap = bootstrap;
	}

	@Override
	public String command() {
		return "remove-project";
	}

	@Override
	public String description() {
		return "  -  remove one project";
	}

	@Override
	public void execute() throws Exception {
		bootstrap.projectService.removeOne();
	}
}

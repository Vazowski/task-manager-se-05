package ru.iteco.taskmanager.command;

import ru.iteco.taskmanager.bootstrap.Bootstrap;

public class FindTaskCommand extends AbstractCommand {

	private Bootstrap bootstrap;
	
	public FindTaskCommand(Bootstrap bootstrap) {
		this.bootstrap = bootstrap;
	}

	@Override
	public String command() {
		return "find-task";
	}

	@Override
	public String description() {
		return "  -  find one task in project";
	}

	@Override
	public void execute() throws Exception {
		bootstrap.taskService.findOne();
	}
}

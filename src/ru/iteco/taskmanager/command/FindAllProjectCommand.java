package ru.iteco.taskmanager.command;

import ru.iteco.taskmanager.bootstrap.Bootstrap;

public class FindAllProjectCommand extends AbstractCommand {

	private Bootstrap bootstrap;
	
	public FindAllProjectCommand(Bootstrap bootstrap) {
		this.bootstrap = bootstrap;
	}

	@Override
	public String command() {
		return "find-all-project";
	}

	@Override
	public String description() {
		return "  -  find all project";
	}

	@Override
	public void execute() throws Exception {
		bootstrap.projectService.findAll();
	}
	
}

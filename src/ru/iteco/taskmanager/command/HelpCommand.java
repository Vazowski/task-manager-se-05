package ru.iteco.taskmanager.command;

import ru.iteco.taskmanager.bootstrap.Bootstrap;

public class HelpCommand extends AbstractCommand {

	private Bootstrap bootstrap;
	
	public HelpCommand(Bootstrap bootstrap) {
		this.bootstrap = bootstrap;
	}
	
	@Override
	public String command() {
		return "help";
	}

	@Override
	public String description() {
		return "  -  show all commands";
	}
	
	@Override
	public void execute() {
		System.out.println();
		for (String key : Bootstrap.commandsMap.keySet()) {
			System.out.println(key + Bootstrap.commandsMap.get(key).description());
		}
	}
}

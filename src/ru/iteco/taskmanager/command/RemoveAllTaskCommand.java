package ru.iteco.taskmanager.command;

import ru.iteco.taskmanager.bootstrap.Bootstrap;

public class RemoveAllTaskCommand extends AbstractCommand {

	private Bootstrap bootstrap;
	
	public RemoveAllTaskCommand(Bootstrap bootstrap) {
		this.bootstrap = bootstrap;
	}

	@Override
	public String command() {
		return "remove-all-task";
	}

	@Override
	public String description() {
		return "  -  remove all task in project";
	}

	@Override
	public void execute() throws Exception {
		bootstrap.taskService.removeAll();
	}
}

package ru.iteco.taskmanager.command;

import ru.iteco.taskmanager.bootstrap.Bootstrap;

public class RemoveAllProjectCommand extends AbstractCommand{

	private Bootstrap bootstrap;
	
	public RemoveAllProjectCommand(Bootstrap bootstrap) {
		this.bootstrap = bootstrap;
	}

	@Override
	public String command() {
		return "remove-project";
	}

	@Override
	public String description() {
		return "  -  remove one project";
	}

	@Override
	public void execute() throws Exception {
		bootstrap.projectService.removeAll();
	}
}

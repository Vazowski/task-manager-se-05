package ru.iteco.taskmanager.command;

import ru.iteco.taskmanager.bootstrap.Bootstrap;

public class RemoveTaskCommand extends AbstractCommand {

	private Bootstrap bootstrap;
	
	public RemoveTaskCommand(Bootstrap bootstrap) {
		this.bootstrap = bootstrap;
	}

	@Override
	public String command() {
		return "remove-task";
	}

	@Override
	public String description() {
		return "  -  remove task from project";
	}

	@Override
	public void execute() throws Exception {
		bootstrap.taskService.removeOne();
	}
}

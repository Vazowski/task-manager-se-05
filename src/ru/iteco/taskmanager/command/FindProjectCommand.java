package ru.iteco.taskmanager.command;

import ru.iteco.taskmanager.bootstrap.Bootstrap;

public class FindProjectCommand extends AbstractCommand {

	private Bootstrap bootstrap;
	
	public FindProjectCommand(Bootstrap bootstrap) {
		this.bootstrap = bootstrap;
	}

	@Override
	public String command() {
		return "find-project";
	}

	@Override
	public String description() {
		return "  -  find one project";
	}

	@Override
	public void execute() throws Exception {
		bootstrap.projectService.findOne();
	}
}

package ru.iteco.taskmanager.command;

import ru.iteco.taskmanager.bootstrap.Bootstrap;

public class PersistTaskCommand extends AbstractCommand {

	private Bootstrap bootstrap;
	
	public PersistTaskCommand(Bootstrap bootstrap) {
		this.bootstrap = bootstrap;
	}

	@Override
	public String command() {
		return "persist-task";
	}

	@Override
	public String description() {
		return "  -  persist task";
	}

	@Override
	public void execute() throws Exception {
		bootstrap.taskService.persist();
	}
}

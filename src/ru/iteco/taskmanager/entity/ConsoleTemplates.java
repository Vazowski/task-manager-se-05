package ru.iteco.taskmanager.entity;

public class ConsoleTemplates {

	public static void isExist(String entity) {
		System.out.println(entity + " with same name exist");
	}
	
	public static void noExist(String entity) {
		System.out.println(entity + " doesn't exist");
	}
	
	public static void isDone() {
		System.out.println("Done");
	}
	
	public static void isEmpty() {
		System.out.println("Empty");
	}
}

package ru.iteco.taskmanager.entity;

public class Entity {

	protected String uuid;
	protected String name;
	protected String description;
	protected String dateBegin;
	protected String dateEnd;
	protected String type;
	
	public Entity() {
		
	}
	
	public String getUuid() {
		return uuid;
	}

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
    	this.name = name;
    }
    
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDateBegin() {
		return dateBegin;
	}

	public String getDateEnd() {
		return dateEnd;
	}
	
	public String getType() {
		return this.type;
	}
}

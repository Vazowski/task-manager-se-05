package ru.iteco.taskmanager.bootstrap;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.command.ExitCommand;
import ru.iteco.taskmanager.command.FindAllProjectCommand;
import ru.iteco.taskmanager.command.FindAllTaskCommand;
import ru.iteco.taskmanager.command.FindProjectCommand;
import ru.iteco.taskmanager.command.FindTaskCommand;
import ru.iteco.taskmanager.command.HelpCommand;
import ru.iteco.taskmanager.command.MergeProjectCommand;
import ru.iteco.taskmanager.command.MergeTaskCommand;
import ru.iteco.taskmanager.command.PersistProjectCommand;
import ru.iteco.taskmanager.command.PersistTaskCommand;
import ru.iteco.taskmanager.command.RemoveAllProjectCommand;
import ru.iteco.taskmanager.command.RemoveAllTaskCommand;
import ru.iteco.taskmanager.command.RemoveProjectCommand;
import ru.iteco.taskmanager.command.RemoveTaskCommand;
import ru.iteco.taskmanager.entity.Entity;
import ru.iteco.taskmanager.repository.ProjectRepository;
import ru.iteco.taskmanager.repository.TaskRepository;
import ru.iteco.taskmanager.service.ProjectService;
import ru.iteco.taskmanager.service.TaskService;

public class Bootstrap {

	private Scanner scanner;
	private String inputCommand;
	
	public static Map<String, AbstractCommand> commandsMap;
	
	public ProjectService projectService = new ProjectService(new ProjectRepository(), new TaskRepository());
	public TaskService taskService  = new TaskService(new TaskRepository());
	
	public void init() {
		commandsMap = new LinkedHashMap<String, AbstractCommand>();
		commandsMap.put("help", new HelpCommand(this));
		commandsMap.put("exit", new ExitCommand(this));
		commandsMap.put("merge-project", new MergeProjectCommand(this));
		commandsMap.put("persist-project", new PersistProjectCommand(this));
		commandsMap.put("find-project", new FindProjectCommand(this));
		commandsMap.put("find-all-project", new FindAllProjectCommand(this));
		commandsMap.put("remove-project", new RemoveProjectCommand(this));
		commandsMap.put("remove-all-project", new RemoveAllProjectCommand(this));
		commandsMap.put("merge-task", new MergeTaskCommand(this));
		commandsMap.put("persist-task", new PersistTaskCommand(this));
		commandsMap.put("find-task", new FindTaskCommand(this));
		commandsMap.put("find-all-task", new FindAllTaskCommand(this));
		commandsMap.put("remove-task", new RemoveTaskCommand(this));
		commandsMap.put("remove-all-task", new RemoveAllTaskCommand(this));
		
    	System.out.println("Enter command (type help for more information)");
    	
    	while(true) {
    		System.out.print("> ");    		
    		scanner = new Scanner(System.in);
    		inputCommand = scanner.nextLine();
    		if (commandsMap.containsKey(inputCommand)) {
    			execute(inputCommand);
    		} else {
    			System.out.println("Command doesn't exist");
    		}
    	}
	}
	
	private static void execute(String command) {
		try {
			commandsMap.get(command.toLowerCase()).execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void showInfo(Entity entity) {
		System.out.println(entity.getType() + " name: " + entity.getName());
		System.out.println(entity.getType() + " description: " + entity.getDescription());
		System.out.println(entity.getType() + " begin date: " + entity.getDateBegin());
		System.out.println(entity.getType() + " end date: " + entity.getDateEnd());
		System.out.println(entity.getType() + " UUID: " + entity.getUuid());
	}
}

package ru.iteco.taskmanager;

import ru.iteco.taskmanager.bootstrap.Bootstrap;

public class App
{
	private static Bootstrap bootstrap;
	
    public static void main( String[] args )
    {
    	bootstrap = new Bootstrap();
    	bootstrap.init();
    }
}

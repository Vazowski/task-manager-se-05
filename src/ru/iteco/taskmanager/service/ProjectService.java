package ru.iteco.taskmanager.service;

import java.util.List;
import java.util.Scanner;
import java.util.UUID;

import ru.iteco.taskmanager.bootstrap.Bootstrap;
import ru.iteco.taskmanager.entity.ConsoleTemplates;
import ru.iteco.taskmanager.repository.ProjectRepository;
import ru.iteco.taskmanager.repository.TaskRepository;

public class ProjectService  {
	
	private String inputName, inputDescription, uuid, findedUuid;
	private Scanner scanner;
	
	private ProjectRepository projectRepository;
	private TaskRepository taskRepository;
	
	public ProjectService(ProjectRepository projectRepository, TaskRepository taskRepository) {
		this.projectRepository = projectRepository;
		this.taskRepository = taskRepository;
		scanner = new Scanner(System.in);
	}
	
	public void merge() {
		System.out.print("Name of project: ");
		inputName = scanner.nextLine();
		System.out.print("Description of project: ");
		inputDescription = scanner.nextLine();
		if (isProjectExist(inputName)) {
			findedUuid = ProjectRepository.findOne(inputName);
			projectRepository.merge(inputName, inputDescription, findedUuid);
		} else {
			uuid = UUID.randomUUID().toString();
			projectRepository.merge(inputName, inputDescription, uuid);
		}
		ConsoleTemplates.isDone();
	}
	
	public void persist() throws Exception {
		System.out.print("Name of project: ");
		inputName = scanner.nextLine();
		if (!isProjectExist(inputName)) {
			System.out.print("Description of project: ");
			inputDescription = scanner.nextLine();
			uuid = UUID.randomUUID().toString();
			projectRepository.merge(inputName, inputDescription, uuid);
			ConsoleTemplates.isDone();
		} else {
			throw new Exception("Project with same name already exist");
		}
	}
	
	public void findOne() throws Exception {
		System.out.print("Name of project: ");
		inputName = scanner.nextLine();
		findedUuid = ProjectRepository.findOne(inputName);
		if (findedUuid.equals("empty")) {
			Bootstrap.showInfo(projectRepository.getProject(findedUuid)); 
		} else {
			throw new Exception("No project with same name");
		}
	}
	
	public void findAll() {
		List<String> tempList = projectRepository.findAll();
		if (tempList.size() == 0) {
			ConsoleTemplates.isEmpty();
			return;
		}
		for (int i = 0; i < tempList.size(); i++) {
			System.out.println("[Project " + (i + 1) + "]");
			Bootstrap.showInfo(projectRepository.getProject(tempList.get(i)));
		}
	}
	
	public void removeOne() {
		System.out.print("Name of project: ");
		inputName = scanner.nextLine();
		findedUuid = ProjectRepository.findOne(inputName);
		if (!findedUuid.equals("empty")) {
			taskRepository.removeAll(findedUuid);
			projectRepository.remove(findedUuid);
			ConsoleTemplates.isDone();
		} else {
			ConsoleTemplates.noExist("Project");
		}
	}
	
	public void removeAll() {
		List<String> tempList = projectRepository.findAll();
		for (int i = 0; i < tempList.size(); i++) {
			taskRepository.removeAll(tempList.get(i));
		}
		projectRepository.removeAll();
		ConsoleTemplates.isDone();
	}
	
	public static boolean isProjectExist(String projectName) {
		return !ProjectRepository.findOne(projectName).equals("empty");
	}
}
